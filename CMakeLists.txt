cmake_minimum_required(VERSION 3.9)

include(add_unkn_libs.cmake)

if ("${USE_UNKN_LIBS}" STREQUAL "")
    message("No Unkn libraries are being used")
else()
    add_unkn_libs("${USE_UNKN_LIBS}")
endif()
