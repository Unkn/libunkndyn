# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set(download_project_cmake ${CMAKE_CURRENT_LIST_DIR}/DownloadProject/DownloadProject.cmake)

function(add_unkn_libs libs)
    message("Searching for inherent dependencies of ${libs}")

    set(do_loop True)
    set(next_libs ${libs})
    set(checked_libs "")
    set(remaining_libs "")

    separate_arguments(next_libs)
    list(REMOVE_DUPLICATES next_libs)

    while(do_loop)
        set(current_libs ${next_libs})
        set(next_libs "")
        separate_arguments(next_libs)

        # TODO _ACM_ Look into the file function to pull deps from files
        foreach(lib ${current_libs})
            set(dep_file ${CMAKE_CURRENT_LIST_DIR}/${lib}/deps.txt)
            message(${dep_file})
            if (EXISTS ${dep_file})
                list(APPEND checked_libs ${lib})
                file(STRINGS ${dep_file} file_deps)
                list(APPEND next_libs ${file_deps})
            else()
                message(FATAL_ERROR "Invalid unkn library '${lib}'")
            endif()
        endforeach()

        list(REMOVE_DUPLICATES next_libs)
        message("Found dependencies: ${next_libs}")
        message("Cumulative libs to include: ${checked_libs}")

        # Strip any libs that dependencies have already been searched for from
        # the list of dependencied found this round
        foreach(lib ${checked_libs})
            list(REMOVE_ITEM next_libs ${lib})
        endforeach(lib)
        message("Next libs to check: ${next_libs}")

        list(LENGTH next_libs len)
        if (${len} EQUAL 0)
            set(do_loop False)
        endif()
    endwhile(do_loop)

    message("All unkn libs being included: ${checked_libs}")

    foreach(lib ${checked_libs})
        message("Adding subdirectory ${lib}")
        add_subdirectory(${lib})
    endforeach(lib)
endfunction(add_unkn_libs)
